package main

import (
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"text/template"

	"github.com/gossie/router"
)

const DEFAULT_PORT = "8080"

//go:embed assets/*
var assets embed.FS

//go:embed templates/*
var htmlTemplates embed.FS

var templates *template.Template

type price struct {
	ArticleId string `json:"articleId"`
	Price     int    `json:"price"`
}

type article struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type fullArticle struct {
	Name        string
	Description string
	Price       string
}

func main() {
	priceServiceHost := os.Getenv("PRICE_SERVICE_HOST")
	if priceServiceHost == "" {
		panic("There is no HOST for the price-service. Set the environment variable PRICE_SERVICE_URL")
	}

	articleServiceHost := os.Getenv("ARTICLE_SERVICE_HOST")
	if articleServiceHost == "" {
		panic("There is no HOST for the article-service. Set the environment variable ARTICLE_SERVICE_URL")
	}

	log.Default().Println("price-service host: ", priceServiceHost)
	log.Default().Println("article-service host: ", articleServiceHost)

	templates = template.Must(template.New("").ParseFS(htmlTemplates, "templates/index.html"))

	httpRouter := router.New()

	httpRouter.Handle("/assets/:dir/:file", http.FileServer(http.FS(assets)))
	httpRouter.Get("/", func(w http.ResponseWriter, r *http.Request, _ router.Context) {
		priceChannel := make(chan []byte, 1)
		priceErrorChannel := make(chan error, 1)
		go callService(priceServiceHost+"/prices", priceChannel, priceErrorChannel)

		articleChannel := make(chan []byte, 1)
		articleErrorChannel := make(chan error, 1)
		go callService(articleServiceHost+"/articles", articleChannel, articleErrorChannel)

		var prices []price
		select {
		case pricesBytes := <-priceChannel:
			json.Unmarshal(pricesBytes, &prices)
		case err := <-priceErrorChannel:
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var articles []article
		select {
		case articleBytes := <-articleChannel:
			json.Unmarshal(articleBytes, &articles)
		case err := <-articleErrorChannel:
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fullarticles := make([]fullArticle, 0, len(articles))
		for i := range articles {
			a := articles[i]
			p, err := findPrice(prices, a.Id)
			if err != nil {
				log.Default().Println("Could not find price for article ID", a.Id)
				fullarticles = append(fullarticles, fullArticle{
					Name:        a.Name,
					Description: a.Description,
					Price:       "Preis konnte nicht ermittelt werden",
				})
			} else {
				fullarticles = append(fullarticles, fullArticle{
					Name:        a.Name,
					Description: a.Description,
					Price:       fmt.Sprintf("%v €", (p.Price)/100.),
				})
			}
		}

		renderTemplate(w, "index", fullarticles)
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Default().Println("running on PORT", port)
	log.Fatal(http.ListenAndServe(":"+port, httpRouter))
}

func renderTemplate(w http.ResponseWriter, tmpl string, data any) {
	err := templates.ExecuteTemplate(w, fmt.Sprintf("%s.html", tmpl), data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func callService(url string, body chan []byte, errorChannel chan error) {
	log.Default().Println("calling", url)
	response, err := http.Get(url)
	if err != nil {
		errorChannel <- err
	}

	bytes, err := io.ReadAll(response.Body)
	if err != nil {
		errorChannel <- err
	}

	body <- bytes
}

func findPrice(prices []price, articleId string) (*price, error) {
	for j := range prices {
		p := prices[j]
		if p.ArticleId == articleId {
			return &p, nil
		}
	}
	return nil, errors.New(fmt.Sprint("could not find price for article ID", articleId))
}
